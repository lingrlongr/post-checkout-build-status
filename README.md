
# What is this?

Have you ever..

- pushed a feature branch which fails a build, only to discover you've branched off a bad ref and the tests have already been fixed on master?
- accidentally performed a release of a bad ref that's failed a build?

If so, this post-checkout hook is for you! Everytime you checkout a ref, this hook will ping your Bamboo or 
Stash server to check the current [build status](https://developer.atlassian.com/stash/docs/latest/how-tos/updating-build-status-for-commits.html) 
of the checked out commit.

# Installation

Requires [Ruby 1.9.3](http://www.ruby-lang.org/en/downloads/) or newer. 

Once Ruby's installed, there's a curlable install script you can use. From the root of your repository just run:

    sh <(curl -s https://bitbucket.org/tpettersen/post-checkout-build-status/raw/master/install.sh)

Follow the prompts, and you're done!

# What does it look like?

    $ git checkout 2.3
    Switched to branch '2.3'
    e1f46e72 has 3 green builds.

    $ git checkout master
    Switched to branch 'master'
    99c42ebd hasn't built yet.

    $ git checkout 2.2
    Switched to branch '2.2'
    Warning! db058541 has 1 red build.

# Uninstalling

From the root of your repository run:

    rm .git/hooks/post-checkout .git/hooks/*-config.yml
