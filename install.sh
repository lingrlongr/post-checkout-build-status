#!/bin/sh

# check for ruby 1.9.3+

rubyver=$(ruby --version)
if [ $? != "0" ]
  then
    echo "Ruby 1.9.3+ is required. See http://www.ruby-lang.org/en/downloads/"
    exit
fi

rubyver=$(expr "$rubyver" : "ruby \([0-9\.]*\)")
if [[ "$rubyver" < "1.9.3" ]]
  then
    echo "Ruby 1.9.3+ is required. You seem to be running ruby $rubyver."
    exit
fi

webroot="https://bitbucket.org/tpettersen/post-checkout-build-status"
hooksdir=.git/hooks
hookscript=$hooksdir/post-checkout

if [ -e $hookscript ]
  then 
    echo "$hookscript already exists! Please remove and re-run."
    exit
  fi

bamboo() {    
  cfgfile=$hooksdir/bamboo-config.yml
  check_config
  read -p "Bamboo URL: " server_url
  write_config
  hookurl=$webroot/raw/master/post-checkout-bamboo.rb
  install_hook
}

stash() {      
  cfgfile=$hooksdir/stash-config.yml
  check_config
  read -p "Stash URL: " server_url
  write_config
  hookurl=$webroot/raw/master/post-checkout-stash.rb
  install_hook
}

install_hook() {
  echo "Installing hook..."
  curl -s $hookurl > $hookscript
  chmod a+x $hookscript
  echo "post-update hook installed. Config written to $cfgfile"  
}

check_config() {
  if [ -e $cfgfile ]
  then 
    echo "$cfgfile already exists! Please remove and re-run."
    exit
  fi
}

write_config() {
  read -p "Username: " username
  read -s -p "Password: " password
  echo
  echo "url: $server_url" >> $cfgfile
  echo "username: $username" >> $cfgfile
  echo "password: $password" >> $cfgfile  
  chmod 700 $cfgfile
}

while true; do
    read -p "Retrieve build status from (S)tash or (B)amboo? " sb
    case $sb in
        [Ss]* ) stash; break;;
        [Bb]* ) bamboo; break;;
        * ) echo "Please answer Stash or Bamboo.";;
    esac
done
